//Copyright (c) 2020-2022 Stefan Thesing
//
//This file is part of Zettels.
//
//Zettels is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Zettels is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Zettels. If not, see http://www.gnu.org/licenses/.

//! Module for all things config

use dirs;
use std::io;
use std::path::{Path, PathBuf};
use libzettels::Config;

// --------------------------------------------------------------------------

/// Set the config dir. If `user_argument` is None, it uses 
/// [dirs::config_dir](https://docs.rs/dirs/1.0.4/dirs/fn.config_dir.html)
/// to get the user's (OS-dependent) config directory and adds `zettels`.
/// to it, e.g. `/home/username/.config/zettels/`. If that path doesn't exist, 
/// it is created.
///
/// # Error
/// Returns a `std::io::Error` if an attempt to create the directory failed.
/// # Panics
/// Panics if `dirs::config_dir` is unable to deliver a user config directory.
pub fn setup_config_file(user_argument: Option<&str>) -> io::Result<PathBuf> {
    match user_argument {
        // User specified something via `--config`
        Some(s) => {
            let config_file = Path::new(s);
            
            // so far so good. Now let's see whether we have something valid
            // 1. If it doesn't exist, return an error.
            if !config_file.exists() {
                return Err(io::Error::new(
                                io::ErrorKind::NotFound, 
                                format!("{:?} doesn't exist.", config_file)
                                ));
            }
            // 2. If it a relative path, make it absolute.
            let config_file = match config_file.is_relative() {
                true => config_file.canonicalize()?,
                false => config_file.to_path_buf(),
            };
                        
            // 3. Is it a file?
            if !config_file.is_file() {
                return Err(io::Error::new(
                                io::ErrorKind::InvalidInput,
                                format!("{:?} is no file.", config_file)
                                ));
            } 
            // 4. Does it have a parent directory?
            if config_file.parent().is_none() {
                return Err(io::Error::new(
                                io::ErrorKind::InvalidInput,
                                format!("{:?} has no parent directory.", 
                                config_file)
                                ));
            }
            // OK, all clear
            return Ok(config_file.to_path_buf());
        },
        // From system standard
        None => {
            if let Some(user_config_dir) = dirs::config_dir() {
                let config_dir = user_config_dir
                                    .join(env!("CARGO_PKG_NAME"));
                
                // create the directory if it doesn't exist
                if !config_dir.exists() {
                    trace!("Config directory doesn't  exist. Attempt to create it.");
                    std::fs::create_dir_all(&config_dir)?;
                }
                
                return Ok(config_dir.join("zettels.cfg"));
            } else {
                println!("Fatal Error: Failed to determine the current user's config \
                          directory.\n\
                          Please consider to file a bug at {}.\n", 
                          env!("CARGO_PKG_HOMEPAGE"));
                panic!("No user config directory.");
            }
        }
    }
}

pub fn show_settings(cfg: &Config) {
    println!("------------------------");
    println!("Root directory: {}", cfg.rootdir.to_str().unwrap());
    println!("Index file: {}", cfg.indexfile.to_str().unwrap());
    println!("Indexing method: {:?}", cfg.indexingmethod);
    println!("Sequence start: {:?}", cfg.sequencestart);
    println!("Ignorefile: {}", cfg.ignorefile.to_str().unwrap());
}
