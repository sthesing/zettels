//Copyright (c) 2020-2022 Stefan Thesing
//
//This file is part of Zettels.
//
//Zettels is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Zettels is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Zettels. If not, see http://www.gnu.org/licenses/.
#![doc(html_logo_url = "https://assets.gitlab-static.net/uploads/-/system/project/avatar/19176983/zettels.png")]
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
extern crate libzettels;
use libzettels::{Config, Index};
// --------------------------------------------------------------------------
extern crate atty;
//#[macro_use] extern crate clap;
extern crate clap;
#[macro_use] extern crate log;
extern crate simplelog;
extern crate tempfile;
extern crate dirs;

use std::path::{PathBuf};
use std::collections::HashSet;

// --------------------------------------------------------------------------
mod args;
mod config;
mod examples;
mod setup;
mod query;
mod logging;

// --------------------------------------------------------------------------

// ##################################
// #   Main and Error Handling      #
// ##################################

fn main() {
    let outcome = run();
    match outcome {
        Ok(()) => std::process::exit(0),
        Err(e) => {
            match e {
                // For now, I'm happy with the default `display` method 
                // from libzettels, so I just use it.
                // But I could do different things with different errors
                // here, either with e or with the several parts of
                // the respective error.
                //Error::BadLink(_,_,_) => error!("{}", e),
                //Error::BadHeader(_,_) => error!("{}", e),
                _ => error!("{:?}", e),
            };
            std::process::exit(1);
        }
    }
}

// ##################################
// #         Program Logic          #
// ##################################

fn run() -> Result<(), libzettels::Error> {
    let app = args::app();
    let matches = app.get_matches();
    
    // For logging, we need a config directory and for almost
    // everything else we need a config file.
    // They might be specified by the user or be defined by the operating
    // system's standards.
    // We don't bother to check whether the "config" option has been used,
    // we just pass the Option-wrapped value to our helper function.
    // So, if the user didn't use the config option, we find `None`. 
    // Thus, `None` means: use the system-standard-directory.
    
    let config_file = config::setup_config_file(matches.value_of("config"))?;
                                                    // might issue an io::Error
    // we can safely unwrap here, because config_file has already been
    // checked if it has a parent.
    let config_dir = config_file.parent().unwrap().to_path_buf();
    
    // Next: logging
    if matches.is_present("mute") {
        logging::setup_logging(0, &config_dir);
    } else {
        // setup according to -v arguments
        logging::setup_logging(matches.occurrences_of("v")+1, &config_dir);
    }
    
    // -----------------------------------------------------------------------
    // Overwriting some values for development and debugging
    // -----------------------------------------------------------------------
    //let tmp_dir = tempfile::tempdir().expect("Failed.");
    //let dir = tmp_dir.path();
    //libzettels::examples::generate_examples_with_config(dir).expect("Failed");
    //let config_dir = dir.join("example_config/libzettels").to_path_buf();
    //let config_file = config_dir.join("libzettels.cfg.yaml").to_path_buf();
    // -----------------------------------------------------------------------
    
    debug!("Config dir: {:?}", config_dir);
    debug!("Config file: {:?}", config_file);
    
    // The one thing that is wholly independent from config:
    // The `examples` subcommand.
    // Only needs logging to be set up.
    // I don't directly check whether it is there (i.e. 
    // `matches.is_present("examples"`)), instead, I see if the
    // the matches of the subcommand is Some. Achieves the same and
    // does the unwrapping in one go.
    if let Some(submatches) = matches.subcommand_matches("examples") {
        examples::go(&submatches);
    }
    
    
    // Now, before we do something with the config, let's see if the user
    // ran the `setup` subcommand.
    //if matches.is_present("setup") {
    if let Some(setup_matches) = matches.subcommand_matches("setup") {
        // show or generate config?
        if setup_matches.is_present("show") {
            println!("Show config from: {:?}", &config_file);
            debug!("Loading config.");
            let cfg = Config::from_file(&config_file)?;
            config::show_settings(&cfg);
        } else {
            setup::generate_settings(config_dir, config_file);
        }        
        std::process::exit(0);
    }
    if !config_file.exists()  {
        // I do not consider this to be an error.
        println!("No config file found. Please run `zettels setup`.");
        std::process::exit(0);
    }
    
    // If we're still here, let's actually get our config.
    debug!("Loading config.");
    let cfg = Config::from_file(&config_file).expect("Failed to load config");
    
    // OK, now we are all set up, let's see what the user wants
    // We'll need the index. Does the index file exist? If not: create it and
    // save it to file.
    if !&cfg.indexfile.exists() {
        info!("Index file does not yet exist. Creating new index.");
        let index = Index::new(&cfg)?;
        index.save(&cfg).expect("Failed to save index to file.");
        info!("Saved new index to {:?}", &cfg.indexfile);
    }
    
    // OK, we're sure to have an index file. Let's load it. Mutably, in 
    // case we need to update.
    let mut index = Index::load(&cfg).expect("Failed to load index.");
    
    // But before we load it, do we need to update it first?
    if matches.is_present("update") {
        info!("Updating index.");
        //index.update(&cfg).expect("Failed to update index");
        index.update(&cfg)?;
        index.save(&cfg).expect("Failed to save index to file.");
        info!("Saved updated index to {:?}", &cfg.indexfile);
    }
    
    // Now, let's see what the user wants. 
    
    // Before anything else, if the user passed `-m`, we don't need to do 
    // anything else, because it would produce output. So:
    if matches.is_present("mute") {
        return Ok(())
    }
    
    // Either it's a use of the "list" subcommand or a regular query
    // Both return results to be processed, so we do it like this:
    
    // List command is present:
    let reverse = matches.is_present("reverse");
    if let Some(list_matches) = matches.subcommand_matches("list") {
        // The two keywords-related commands are incompatible with other 
        // list commands:
        if list_matches.is_present("list_keywords") {
            let keywords = &index.get_keywords();
            for keyword in keywords {
                println!("{}", keyword);
            }
        } else if list_matches.is_present("list_keyword_count") {
            for (keyword, count) in &index.count_keywords() {
                println!("{:<4} | {}", count, keyword);
            }        
        } else {
        // The other two can be combined, so we either have 
        // titles, filenames or both (default)            
            // 1. filenames only
            if list_matches.is_present("list_files") &&
               !list_matches.is_present("list_titles") {
                let results = query::get_files(&index);
                process_output(
                    &index,
                    results,
                    OutputConf {
                        pretty: false,
                        reverse,
                    },
                );
            // 2. titles only
            } else if list_matches.is_present("list_titles") &&
                !list_matches.is_present("list_files") {
                for zettel in index.files.values() {
                    println!("{}", zettel.title);
                }
            // 3. both (default fallback)
            } else {
                let results = query::get_files(&index);
                process_output(
                    &index,
                    results,
                    OutputConf {
                        pretty: true,
                        reverse,
                    },
                );
            }
        }
    // List command is not present:
    } else {    
        //Run the queries, which are defined in `matches`
        let (results, mut pretty) = query::query(&index, &matches, &cfg);
        // We possibly got an implied "pretty"-flag with the query result.
        // However, if the user has explicitly set the pretty flag, we use 
        // that, no matter what.
        if matches.is_present("pretty") {
            pretty = true;
        }
        // OK, we're done. Let's output what we have.
        process_output(&index, results, OutputConf { pretty, reverse });
    };
        
    // If nothing went wrong until now, everything important has already
    // happened.
    Ok(())
}

struct OutputConf {
    pretty: bool,
    reverse: bool,
}

fn process_output(index: &Index, results: HashSet<PathBuf>, conf: OutputConf) {
    let zettels: Vec<(String, String)> = extract_title_and_key(index, results, conf.reverse);
    print_output(zettels, conf.pretty);
}

fn extract_title_and_key(index: &Index, results: HashSet<PathBuf>, reverse: bool) -> Vec<(String, String)> {
    let mut zettels: Vec<(String, String)> = Vec::new();
    for r in results {
        if let Some(zettel) = index.get_zettel(&r) {
            if let Some(key) = r.to_str() {
                zettels.push((zettel.title.clone(), key.to_string().clone()));
            }
        }
    }
    if reverse {
        zettels.sort_by(|a, b| b.0.cmp(&a.0));
    } else {
        zettels.sort_by(|a, b| a.0.cmp(&b.0));
    }
    zettels
}

fn print_output(output: Vec<(String, String)>, pretty: bool) {
    for (title, key) in output {
        if pretty {
            println!("{:<50} | {}", title, key);
        } else {
            println!("{}", key);
        }
    }
}

#[cfg(test)]
mod tests {
    use std::{collections::BTreeMap, time::SystemTime};

    use super::*;
    use libzettels::{Index, Zettel};

    #[test]
    fn test_title_and_key_extraction() {
        // Setup
        let mut test_tree = BTreeMap::new();
        test_tree.insert(PathBuf::from("/path/to/zettel3"), Zettel::new("title3"));
        test_tree.insert(PathBuf::from("/path/to/zettel1"), Zettel::new("title1"));
        test_tree.insert(PathBuf::from("/path/to/zettel2"), Zettel::new("title2"));
        let test_index: Index = Index {
            files: test_tree,
            timestamp: SystemTime::now(),
        };
        let mut test_results: HashSet<PathBuf> = HashSet::new();
        test_results.insert(PathBuf::from("/path/to/zettel2"));
        test_results.insert(PathBuf::from("/path/to/zettel1"));
        test_results.insert(PathBuf::from("/path/to/zettel3"));
        let test_zettels: Vec<(String, String)> =
            vec![
                ("title1".to_string(), "/path/to/zettel1".to_string()),
                ("title2".to_string(), "/path/to/zettel2".to_string()),
                ("title3".to_string(), "/path/to/zettel3".to_string())
            ];
        // Assert
        assert_eq!(
            test_zettels,
            extract_title_and_key(&test_index, test_results, false)
        );
    }

    #[test]
    fn test_title_and_key_extraction_reverse() {
        // Setup
        let mut test_tree = BTreeMap::new();
        test_tree.insert(PathBuf::from("/path/to/zettel3"), Zettel::new("title3"));
        test_tree.insert(PathBuf::from("/path/to/zettel1"), Zettel::new("title1"));
        test_tree.insert(PathBuf::from("/path/to/zettel2"), Zettel::new("title2"));
        let test_index: Index = Index {
            files: test_tree,
            timestamp: SystemTime::now(),
        };
        let mut test_results: HashSet<PathBuf> = HashSet::new();
        test_results.insert(PathBuf::from("/path/to/zettel2"));
        test_results.insert(PathBuf::from("/path/to/zettel1"));
        test_results.insert(PathBuf::from("/path/to/zettel3"));
        let test_zettels: Vec<(String, String)> =
            vec![
                ("title3".to_string(), "/path/to/zettel3".to_string()),
                ("title2".to_string(), "/path/to/zettel2".to_string()),
                ("title1".to_string(), "/path/to/zettel1".to_string())
            ];
        // Assert
        assert_eq!(
            test_zettels,
            extract_title_and_key(&test_index, test_results, true)
        );
    }

}
